@extends('panel::layouts.main')

@section('title', $department->id ? trans('index.'. $entity . '.edit') : trans('index.'. $entity . '.create'))

@section('actions')
    <a href="{{ url()->previous() }}" class="btn btn-default js_panel_form-ajax-back pjax-link"><i class="fa fa-chevron-left"></i> Вернуться назад</a>
@endsection

@section('main')

    {!! Form::model($department, ['route' => 'index.'. $entity . '.store', 'class' => 'ibox form-horizontal js_panel_form-ajax js_panel_form-ajax-reset'])  !!}
    {!! Form::hidden('id') !!}
        <ul class="nav nav-tabs nav-products1" role="tablist">
            @include('index.'. $entity . '.edit.nav')
        </ul>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        @include('index.'. $entity . '.edit.tab-general')
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-footer">
            {{ Form::panelButton() }}
        </div>
    {!! Form::close() !!}
@endsection