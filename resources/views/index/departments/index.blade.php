@extends('panel::layouts.main')

@section('title', trans('index.'. $entity . '.title'))

@section('actions')
    <a href="{{ route('index.'. $entity . '.create') }}" class="btn btn-sm btn-primary pjax-link"><span class="fa fa-plus"></span> {{ trans('index.'. $entity . '.add_button') }}</a>
@endsection

@section('main')
    <div class="ibox">
        <div class="ibox-content">
            <h2>{{ trans('index.'. $entity . '.list') }}</h2>
            <div class="hr-line-dashed"></div>
            @include('index.'. $entity . '.index.filter')
            <div class="hr-line-dashed"></div>
            <div class="js_table-wrapper">
                @include('index.'. $entity . '.index.table')
            </div>
        </div>
        <div class="ibox-footer js_table-pagination">
            @include('index.partials.pagination', ['paginator' => $departments])
        </div>
    </div>
@endsection