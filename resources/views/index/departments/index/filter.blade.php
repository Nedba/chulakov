{!! Form::open(['class' => 'form-inline js_table-search', 'method' => 'get']) !!}
    <div class="form-group">
        {!! Form::text('name_department', request('name_department'), ['placeholder' => 'Введите название', 'class' => 'form-control']) !!}
    </div>
    {{--<div class="form-group">--}}
        {{--{!! Form::select('category_id', $categoriesSelect, request('category_id'), ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
    <a href="#" class="btn btn-default js_table-reset"><span class="fa fa-ban"></span> Сбросить</a>
{!! Form::close() !!}