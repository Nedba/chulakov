@if($departments->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ trans('index_labels.name_department') }}</th>
            <th class="text-center">{{ trans('index_labels.count_employees') }}</th>
            <th class="text-center">{{ trans('index_labels.max_salary') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($departments as $department)
            <tr>
                <td>{{ $department->id }}</td>
                <td><a href="{{route ('index.' . $entity . '.edit', $department)}}" class="pjax-link">{{ $department->name_department }}</a></td>
                <td class="text-center">{{ $department->employees_count }}</td>
                <td class="text-center">@price($department->max_salary)</td>
                <td class="td-actions">
                    <a href="{{route ('index.' . $entity . '.edit', $department)}}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route ('index.' . $entity . '.delete', $department)}}" class="btn btn-sm btn-danger js_panel_remove data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif