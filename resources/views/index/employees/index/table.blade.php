@if($employees->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ trans('index_labels.full_name') }}</th>
            <th>{{ trans('index_labels.sex') }}</th>
            <th class="text-center">{{ trans('index_labels.salary') }}</th>
            <th>{{ trans('index.departments.title') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td><a href="{{route ('index.' . $entity . '.edit', $employee)}}" class="pjax-link">{{ $employee->fullName }}</a></td>
                <td>{{ trans('index_labels.radio_sex.'. $employee->sex) }}</td>
                <td class="text-center">@price($employee->salary)</td>
                <td>{{ $employee->departments->implode('name_department', ', ') }}</td>
                <td class="td-actions">
                    <a href="{{route ('index.' . $entity . '.edit', $employee)}}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route ('index.' . $entity . '.delete', $employee)}}" class="btn btn-sm btn-danger js_panel_remove" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif