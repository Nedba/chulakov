{!! Form::open(['class' => 'form-inline js_table-search', 'method' => 'get']) !!}
    <div class="form-group">
        {!! Form::text('first_name', request('first_name'), ['placeholder' => 'Введите имя', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::text('last_name', request('last_name'), ['placeholder' => 'Введите фамилию', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::text('middle_name', request('middle_name'), ['placeholder' => 'Введите отчество', 'class' => 'form-control']) !!}
    </div>
    <a href="#" class="btn btn-default js_table-reset"><span class="fa fa-ban"></span> Сбросить</a>
{!! Form::close() !!}