<div role="tabpanel" class="tab-pane" id="departments">
    <h2>{{ trans('index.departments.title') }}</h2>
    <div class="hr-line-dashed"></div>
    <div class="row">
        @foreach($departments as $department)
            <div class="col-md-6">
                <div class="product-checkbox m-b-sm">
                    <div class="checkbox">
                        {{ Form::checkbox('departments['. $department->id .']', 1, $employee->departments->contains($department->id), ['id' => 'departments['. $department->id .']']) }}
                        {{ Form::label('departments['. $department->id .']', $department->name_department) }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>