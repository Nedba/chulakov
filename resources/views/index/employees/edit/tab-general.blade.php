<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::panelText('first_name') }}
            {{ Form::panelText('last_name') }}
            {{ Form::panelText('middle_name') }}
            {{ Form::panelText('salary') }}
        </div>
        <div class="col-md-6">
            {{ Form::panelRadio('sex', $employee->sex, 'radio_sex') }}
        </div>
    </div>
</div>