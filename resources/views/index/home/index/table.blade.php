<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th></th>
            @foreach($departments as $department)
                <th>{{ $department->name_department }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            <tr>
                <td>{{ $employee->full_name }}</td>
                @foreach($departments as $department)
                    <td>{{ $employee->departments->contains($department->id) ? '+' : '-' }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>