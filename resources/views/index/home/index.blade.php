@extends('panel::layouts.main')

@section('title', trans('index.'. $entity . '.title'))

@section('main')
    <div class="ibox">
        <div class="ibox-content">
            <h2>{{ trans('index.'. $entity . '.grid') }}</h2>
            <div class="hr-line-dashed"></div>
            @include('index.'. $entity . '.index.table')
        </div>
    </div>
@endsection