<div class="form-group">
    {!! Form::label($name. '[]', trans('index_labels.'. $name), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($name. ($multiply ? '[]' : ''), $values, $selected, ['class' => 'form-control '. $class, $multiply ? 'multiple' : null]) !!}
        <p class="error-block"></p>
    </div>
</div>