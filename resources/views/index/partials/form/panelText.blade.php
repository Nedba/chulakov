@php($label = str_replace('[', '.', $name))
@php($label = str_replace(']', '', $label))

<div class="form-group">
    {!! Form::label($name, trans('index_labels.'. $label), ['class' => ($col ? 'col-md-4' : '' . ' control-label')]) !!}
    <div class="{{ $col ? 'col-md-8' : '' }}">
        {!! Form::text($name, $val, ['class' => 'form-control '. $class]) !!}
        <p class="error-block"></p>
    </div>
</div>