<div class="form-group">
    {{ Form::label($name .'-yes', trans('index_labels.'. $name), ['class' => 'control-label col-md-4']) }}
    <div class="col-md-8">
        <div class="radio radio-warning radio-inline">
            {{ Form::radio($name, 1, null, ['id' => $name .'-yes']) }}
            <label for="{{ $name }}-yes">{{ trans('index_labels.'. $label .'.1') }}</label>
        </div>
        <div class="radio radio-danger radio-inline">
            {{ Form::radio($name, 0, !$selected ? true : false, ['id' => $name.'-no']) }}
            <label for="{{ $name }}-no">{{ trans('index_labels.'. $label .'.0') }}</label>
        </div>
    </div>
</div>