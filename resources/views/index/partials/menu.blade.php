<li class="{{ request()->url() == route('index.home') ? 'active' : '' }}">
    <a href="{{ route('index.home') }}" class="pjax-link">
        <i class="fa fa-home"></i>
        <span class="text">  {{ trans('index.home.title') }}</span>
    </a>
</li>

<li class="{{ str_contains(request()->url(), route('index.employees.index')) ? 'active' : '' }}">
    <a href="{{ route('index.employees.index') }}" class="pjax-link">
        <i class="fa fa-users"></i>
        <span class="text"> {{ trans('index.employees.title') }}</span>
    </a>
</li>

<li class="{{ str_contains(request()->url(), route('index.departments.index')) ? 'active' : '' }}">
    <a href="{{ route('index.departments.index') }}" class="pjax-link">
        <i class="fa fa-list"></i>
        <span class="text">  {{ trans('index.departments.title') }}</span>
    </a>
</li>