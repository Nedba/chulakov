<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-warning " href="#"><i class="fa fa-bars"></i> </a>
            <div class="input-group-btn add-button-top">
                <button data-toggle="dropdown" class="btn btn-sm btn-white dropdown-toggle" type="button" aria-expanded="false"><span class="fa fa-plus"></span> Добавить <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{ route('index.employees.create') }}" class="pjax-link"> {{ trans('index.departments.add_button') }}</a></li>
                    <li><a href="{{ route('index.departments.create') }}" class="pjax-link"> {{ trans('index.employees.add_button') }}</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            {{--<li>--}}
                {{--<a href="{{ route('index.auth.logout') }}">--}}
                    {{--<i class="fa fa-sign-out"></i> Выйти--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>
    </nav>
</div>