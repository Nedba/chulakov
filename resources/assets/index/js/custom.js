$(document).ready(function () {

    $(document).on('input', '.js_table-search :input', searchTable);
    $(document).on('submit', '.js_table-search', searchTable);
    $(document).on('click', '.js_table-pagination a', paginateTable);
    $(document).on('click', '.js_table-reset', resetSearchTable);
    $(document).on('panel-form-ajax-success', '.js_panel_form-ajax', resetAjaxForm);
    $(document).on('click', '.js_panel_remove', removeElement);

    $(document).on('click', '#side-menu .pjax-link', activeMenu);
    $(document).pjax('.pjax-link', '#pjax-container', {fragment: '#pjax-container', timeout: 5000});

    function removeElement(e) {
        e.preventDefault();
        let $link = $(this);
        let url = $link.attr('href');
        bootbox.confirm({
            message: 'Вы действительно хотите удалить?',
            callback: function (result) {
                if (result) {
                    $.get(url, (data) => {
                        if (data.result == 'success') {
                            $link.closest('tr').remove();
                            window.showNotification('Удаление прошло успешно', 'error');
                        } else {
                            window.showNotification(data.message, 'error');
                        }
                    })
                }
            }
        })
        return false;
    }
    
    function activeMenu() {
        $('#side-menu li').not('.sub-menu').removeClass('active');
        $(this).closest('li').addClass('active');
    }

    $('#pjax-container').on('pjax:start', () => {
        $('.wrapper-spinner').show();
    })

    $('#pjax-container').on('pjax:complete', () => {
        $('.wrapper-spinner').hide();
    })

    function resetAjaxForm() {
        if ($(this).hasClass('js_panel_form-ajax-reset')) {
            setTimeout(() => $('.js_panel_form-ajax-back').click(), 1000);
        }
    }

    function searchTable(e) {
        e.preventDefault();
        let $form = $('.js_table-search');
        history.pushState({}, '', '?' + $form.serialize());
        $form.ajaxSubmit({
            success: function (data) {
                renderData(data);
            }
        });
        return false;
    }

    function paginateTable(e) {
        e.preventDefault();
        let link = $(this).attr('href');
        $.get(link, function (data) {
            renderData(data);
            $.scrollTo($('.ibox-content'), 400);
        });
        return false;
    }

    function renderData(data) {
        let $table = $('.js_table-wrapper');
        let $pagination = $('.js_table-pagination');
        $table.html(data.view);
        $pagination.html(data.pagination);
    }

    function resetSearchTable(e) {
        e.preventDefault();
        let $form = $('.js_table-search');
        $form.find('input').val('');
        $form.find('textarea').val('');
        $form.find('select').prop('selectedIndex', 0);
        searchTable(e);
        return false;
    }

});

$(document).ajaxComplete(() => {
    $('[data-toggle="tooltip"]').tooltip();
});