<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{

    public function boot()
    {

        Form::macro('labelHtml', function ($name, $value = null, $options = array()) {
            return htmlspecialchars_decode(Form::label($name, $value, $options));
        });

        //Admin
        Form::component('onOffCheckbox', 'index.partials.form.onOffCheckbox', ['name', 'checked', 'id']);
        Form::component('panelText', 'index.partials.form.panelText', ['name', 'val', 'class', 'col' => true]);
        Form::component('panelTextarea', 'index.partials.form.panelTextarea', ['name', 'redactor' => false, 'type' => null, 'label' => null]);
        Form::component('panelRadio', 'index.partials.form.panelRadio', ['name', 'selected' => false, 'label' => 'radio']);
        Form::component('panelSelect', 'index.partials.form.panelSelect', ['name', 'values', 'selected', 'class', 'multiply']);
        Form::component('panelButton', 'index.partials.form.panelButton', []);
        Form::component('goToUrl', 'index.partials.form.panelGoToUrl', ['url']);
        
    }

    public function register()
    {

    }
}