<?php

namespace App\Providers;

use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
	protected $namespace = 'App\Http\Controllers';

	public function boot()
	{

		parent::boot();
	}

	public function map()
	{
		$this->mapIndexRoutes();
	}

	protected function mapIndexRoutes()
	{
		Route::group([
			'middleware' => 'web',
			'namespace' => $this->namespace . '\Index',
			'as' => 'index.'
		], function ($router) {
			require base_path('routes/index.php');
		});
	}
}
