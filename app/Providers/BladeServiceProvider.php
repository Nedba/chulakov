<?php

namespace App\Providers;

use App\Services\Prettifier;
use Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Blade::directive('dd', function($data){
            return "<?php dd(with($data)) ;?>";
        });
        
        Blade::directive('dump', function($data){
            return "<?php dump(with($data)) ;?>";
        });

        Blade::directive('price', function($price){
            return '<?= \App\Services\Prettifier::prettifyPrice(with(' . $price . ')) ?>';
        });
    }
    
    public function register()
    {
        //
    }
}
