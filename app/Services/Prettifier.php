<?php

namespace App\Services;


class Prettifier
{
    public static function prettifyPrice($price)
    {
        return number_format($price, 0, ',', ' ') . ' руб.';
    }
}