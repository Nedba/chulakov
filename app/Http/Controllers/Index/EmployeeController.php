<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Http\Requests\Index\EmployeeRequest;
use App\Models\Employee;
use App\Models\Department;

class EmployeeController extends Controller
{
    protected $entity = 'employees';

    public function index()
    {
        $employees = Employee::filter(request()->all())->with('departments')->latest()->paginate();
        if(request()->ajax() && !request('_pjax')) return $this->ajaxView($employees);
        return view('index.' . $this->entity . '.index', compact('employees') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $employee = new Employee();
        $departments = Department::orderBy('name_department')->get();
        return view('index.' . $this->entity . '.edit', compact('employee', 'departments') + ['entity' => $this->entity]);
    }

    public function edit(Employee $employee)
    {
		$departments = Department::orderBy('name_department')->get();
        return view('index.' . $this->entity . '.edit', compact('employee', 'departments') + ['entity' => $this->entity]);
    }

    public function store(EmployeeRequest $request)
    {
        if ($id = request('id')) {
            $employee = Employee::find($id);
            $employee->update(request()->except('departments'));
        } else {
            $employee = Employee::create(request()->except('departments'));
        }
        $employee->departments()->sync(collect(request('departments', []))->keys()->toArray());

        return $this->responseSuccess();
    }

    public function delete(Employee $employee)
    {
        $employee->delete();
        return $this->responseSuccess();
    }

    protected function ajaxView($employees){
        return response([
            'view' => view('index.' . $this->entity . '.index.table', compact('employees') + ['entity' => $this->entity])->render(),
            'pagination' => view('index.partials.pagination', ['paginator' => $employees])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}