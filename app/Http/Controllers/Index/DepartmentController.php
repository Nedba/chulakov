<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Http\Requests\Index\DepartmentRequest;
use App\Models\Department;

class DepartmentController extends Controller
{
    protected $entity = 'departments';

    public function index()
    {
        $departments = Department::filter(request()->all())
			->select(['*'])
			->employeesCount()
			->maxSalary()
			->latest()
			->paginate();
        if(request()->ajax() && !request('_pjax')) return $this->ajaxView($departments);
        return view('index.' . $this->entity . '.index', compact('departments') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $department = new Department();
        return view('index.' . $this->entity . '.edit', compact('department') + ['entity' => $this->entity]);
    }

    public function edit(Department $department)
    {
        return view('index.' . $this->entity . '.edit', compact('department') + ['entity' => $this->entity]);
    }

    public function store(DepartmentRequest $request)
    {
        if ($id = request('id')) {
            $department = Department::find($id);
            $department->update(request()->except('departments'));
        } else {
            Department::create(request()->except('departments'));
        }

        return $this->responseSuccess();
    }

    public function delete(Department $department)
    {
    	if (!$department->employees()->count()) {
			$department->delete();
			return $this->responseSuccess();
		}
		return $this->responseError(['message' => trans('validation.custom.departments.no_remove_department')]);
    }

    protected function ajaxView($departments){
        return response([
            'view' => view('index.' . $this->entity . '.index.table', compact('departments') + ['entity' => $this->entity])->render(),
            'pagination' => view('index.partials.pagination', ['paginator' => $departments])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}