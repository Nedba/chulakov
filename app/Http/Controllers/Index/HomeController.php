<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Employee;

class HomeController extends Controller
{
    protected $entity = 'home';

    public function index()
    {
    	$departments = Department::orderBy('name_department')->get();
    	$employees = Employee::with('departments')->get();

        return view('index.' . $this->entity . '.index', compact('departments', 'employees') + ['entity' => $this->entity]);
    }
}