<?php

namespace App\Http\Requests\Index;

use App\Http\Requests\Request;

class DepartmentRequest extends Request
{
    public function rules()
    {
        return [
        	'name_department' => 'required',
		];
    }

    public function attributes()
    {
        return [
            'name_department' => trans('index_labels.name_department'),
        ];
    }
}