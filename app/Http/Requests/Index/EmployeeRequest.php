<?php

namespace App\Http\Requests\Index;

use App\Http\Requests\Request;

class EmployeeRequest extends Request
{
    public function rules()
    {
        return [
        	'first_name' => 'required',
        	'last_name' => 'required',
        	'middle_name' => 'required',
        	'salary' => 'required|integer',
        	'departments' => 'required|array',
		];
    }

    public function attributes()
    {
        return [
            'first_name' => trans('index_labels.first_name'),
            'last_name' => trans('index_labels.last_name'),
            'middle_name' => trans('index_labels.middle_name'),
            'salary' => trans('index_labels.salary'),
        ];
    }

	public function messages()
	{
		return [
//			'departments.required' => 'Нельзя создать сотрудника не указав ему хотя бы один отдел.'
		];
    }
}