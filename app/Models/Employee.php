<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	protected $fillable = [
		'first_name',
		'last_name',
		'middle_name',
		'salary',
		'sex'
	];

	//Relationships
	public function departments()
	{
		return $this->belongsToMany(Department::class);
	}

	//Mutators
	public function getFullNameAttribute()
	{
		return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
	}

	//Scopes
	public function scopeFilter($query, $data)
	{
		$query
			->when($firstName = array_get($data, 'first_name'), function ($q) use ($firstName) {
				return $q->where('first_name', 'like', "%$firstName%");
			})
			->when($lastName = array_get($data, 'last_name'), function ($q) use ($lastName) {
				return $q->where('last_name', 'like', "%$lastName%");
			})
			->when($middleName = array_get($data, 'middle_name'), function ($q) use ($middleName) {
				return $q->where('middle_name', 'like', "%$middleName%");
			});
		return $query;
	}
}
