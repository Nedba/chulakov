<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;

class Department extends Model
{
	protected $fillable = [
		'name_department',
	];

	//Relationships
	public function employees()
	{
		return $this->belongsToMany(Employee::class);
	}

	//Scopes
	public function scopeFilter($query, $data)
	{
		$query
			->when($name = array_get($data, 'name_department'), function ($q) use ($name) {
				return $q->where('name_department', 'like', "%$name%");
			});
		return $query;
	}

	public function scopeMaxSalary($query)
	{
		$query
			->selectSub(
				$this
					->employees()
					->getRelationQuery(
						$this->employees()->getRelated()->newQuery(),
						$query)
					->select(new Expression('COALESCE(max(salary), 0)'))
					->toBase(),
				'max_salary'
			);
	}

	public function scopeEmployeesCount($query)
	{
		$query
			->selectSub(
				$this
					->employees()
					->getRelationQuery(
						$this->employees()->getRelated()->newQuery(),
						$query)
					->select(new Expression('COALESCE(count(*), 0)'))
					->toBase(),
				'employees_count'
			);
	}
}
