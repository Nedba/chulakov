const elixir = require('secret-elixir');

let
    resources = './resources/assets/',
    public = './public/assets/',
    bower = './bower_components/';

elixir(mix => {
    mix
        .less(`${resources}index/less/*.less`, `${public}index/css/styles.css`)

        .scripts([
            `${bower}jquery-pjax/jquery.pjax.js`,
            `${bower}jquery.scrollTo/jquery.scrollTo.min.js`,
        ], `${public}index/js/libs.js`)

        .browserify(`${resources}index/js/index.js`, `${public}index/js/scripts.js`)
});