<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    public function run()
    {
        foreach (Department::all() as $item) {
            $item->delete();
        }

        $items = [
        	'Отдел закупок',
			'Отдел продаж',
			'PR-отдел',
		];

		foreach ($items as $item) {
			Department::create([
				'name_department' => $item,
			]);
        }
    }
}
