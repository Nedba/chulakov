<?php

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    public function run()
    {
        foreach (Employee::all() as $item) {
            $item->delete();
        }

		$this->faker = Faker\Factory::create('ru_RU');

		$departments = \App\Models\Department::all();

		factory(Employee::class, 50)->create()->each(function (Employee $employee) use ($departments) {
			$employee->departments()->sync($departments->random(rand(1, 3))->pluck('id')->toArray());
		});
    }
}
