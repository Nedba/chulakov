<?php

$factory->define(App\Models\Employee::class, function (Faker\Generator $faker) {
    return [
		'first_name' => $faker->firstName,
		'middle_name' => $faker->lastName,
		'last_name' => $faker->lastName,
		'sex' => rand(0, 1),
		'salary' => collect(range(60, 400, 20))->random(),
    ];
});