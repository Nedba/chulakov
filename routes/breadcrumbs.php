<?php

Breadcrumbs::register('index.home', function($breadcrumbs) {
    $breadcrumbs->push('Главная', route('index.home'));
});

//Departments
Breadcrumbs::register('index.departments.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(trans('index.departments.title'), route('index.departments.index'));
});
Breadcrumbs::register('index.departments.create', function($breadcrumbs) {
    $breadcrumbs->parent('index.departments.index');
    $breadcrumbs->push(trans('index.departments.create'), route('index.departments.create'));
});
Breadcrumbs::register('index.departments.edit', function($breadcrumbs, $departments) {
    $breadcrumbs->parent('index.departments.index');
    $breadcrumbs->push(trans('index.departments.edit'). ' '.  $departments->first_name, route('index.departments.edit', $departments));
});

//Employees
Breadcrumbs::register('index.employees.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(trans('index.employees.title'), route('index.employees.index'));
});
Breadcrumbs::register('index.employees.create', function($breadcrumbs) {
    $breadcrumbs->parent('index.employees.index');
    $breadcrumbs->push(trans('index.employees.create'), route('index.employees.create'));
});
Breadcrumbs::register('index.employees.edit', function($breadcrumbs, $employee) {
    $breadcrumbs->parent('index.employees.index');
    $breadcrumbs->push(trans('index.employees.edit'). ' '.  $employee->title, route('index.employees.edit', $employee));
});