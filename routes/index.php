<?php

Route::get('/', 'HomeController@index')->name('home');

//Departments
Route::group(['prefix' => 'departments', 'as' => 'departments.'], function () {
	Route::get('/', 'DepartmentController@index')->name('index');
	Route::get('create', 'DepartmentController@create')->name('create');
	Route::post('store', 'DepartmentController@store')->name('store');
	Route::group(['prefix' => '{department}'], function () {
		Route::get('edit', 'DepartmentController@edit')->name('edit');
		Route::get('delete', 'DepartmentController@delete')->name('delete');
	});
});

//Employees
Route::group(['prefix' => 'employees', 'as' => 'employees.'], function () {
	Route::get('/', 'EmployeeController@index')->name('index');
	Route::get('create', 'EmployeeController@create')->name('create');
	Route::post('store', 'EmployeeController@store')->name('store');
	Route::group(['prefix' => '{employee}'], function () {
		Route::get('edit', 'EmployeeController@edit')->name('edit');
		Route::get('delete', 'EmployeeController@delete')->name('delete');
	});
});


